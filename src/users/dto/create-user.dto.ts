import {ApiProperty} from '@nestjs/swagger'
import {IsEmail, IsString, Length} from 'class-validator'

export class CreateUserDto {
    @ApiProperty({example: 'user@email.com', description: 'Email'})
    @IsString({message: 'Must be string'})
    @IsEmail({}, {message: 'Invalid email'})
    readonly email: string

    @ApiProperty({example: 'password123', description: 'Password'})
    @IsString({message: 'Must be string'})
    @Length(6, 16, {message: 'Password must be between 6 and 16 chars'})
    readonly password: string
}
